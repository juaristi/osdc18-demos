#ifndef NET_H
#define NET_H
#include <netinet/in.h>
#include "common.h"

struct socket
{
	int sock;
	int sock_child;

	in_addr_t src_ipv4;
	in_addr_t dst_ipv4;
	in_port_t src_port;
	in_port_t dst_port;
};

int server_accept(struct config *, struct socket *);
int client_connect(struct config *, struct socket *);

#endif
