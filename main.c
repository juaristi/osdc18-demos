#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <getopt.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <gnutls/gnutls.h>
#include "net.h"
#include "sqlite.h"
#include "common.h"
#include "errors.h"
#include "credentials.h"

#define DEFAULT_IP_ADDR		"127.0.0.1"
#define DEFAULT_PORT		8080

static unsigned char server_cert_pem[] =
"-----BEGIN CERTIFICATE-----\n"
"MIIDIzCCAgugAwIBAgIMUz8PCR2sdRK56V6OMA0GCSqGSIb3DQEBCwUAMA8xDTAL\n"
"BgNVBAMTBENBLTEwIhgPMjAxNDA0MDQxOTU5MDVaGA85OTk5MTIzMTIzNTk1OVow\n"
"EzERMA8GA1UEAxMIc2VydmVyLTIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK\n"
"AoIBAQDZ3dCzh9gOTOiOb2dtrPu91fYYgC/ey0ACYjQxaru7FZwnuXPhQK9KHsIV\n"
"YRIyo49wjKZddkHet2sbpFAAeETZh8UUWLRb/mupyaSJMycaYCNjLZCUJTztvXxJ\n"
"CCNfbtgvKC+Vu1mu94KBPatslgvnsamH7AiL5wmwRRqdH/Z93XaEvuRG6Zk0Sh9q\n"
"ZMdCboGfjtmGEJ1V+z5CR+IyH4sckzd8WJW6wBSEwgliGaXnc75xKtFWBZV2njNr\n"
"8V1TOYOdLEbiF4wduVExL5TKq2ywNkRpUfK2I1BcWS5D9Te/QT7aSdE08rL6ztmZ\n"
"IhILSrMOfoLnJ4lzXspz3XLlEuhnAgMBAAGjdzB1MAwGA1UdEwEB/wQCMAAwFAYD\n"
"VR0RBA0wC4IJbG9jYWxob3N0MA8GA1UdDwEB/wQFAwMHoAAwHQYDVR0OBBYEFJXR\n"
"raRS5MVhEqaRE42A3S2BIj7UMB8GA1UdIwQYMBaAFP6S7AyMRO2RfkANgo8YsCl8\n"
"JfJkMA0GCSqGSIb3DQEBCwUAA4IBAQCQ62+skMVZYrGbpab8RI9IG6xH8kEndvFj\n"
"J7wBBZCOlcjOj+HQ7a2buF5zGKRwAOSznKcmvZ7l5DPdsd0t5/VT9LKSbQ6+CfGr\n"
"Xs5qPaDJnRhZkOILCvXJ9qyO+79WNMsg9pWnxkTK7aWR5OYE+1Qw1jG681HMkWTm\n"
"nt7et9bdiNNpvA+L55569XKbdtJLs3hn5gEQFgS7EaEj59aC4vzSTFcidowCoa43\n"
"7JmfSfC9YaAIFH2vriyU0QNf2y7cG5Hpkge+U7uMzQrsT77Q3SDB9WkyPAFNSB4Q\n"
"B/r+OtZXOnQhLlMV7h4XGlWruFEaOBVjFHSdMGUh+DtaLvd1bVXI\n"
"-----END CERTIFICATE-----\n"
"-----BEGIN CERTIFICATE-----\n"
"MIIDATCCAemgAwIBAgIBATANBgkqhkiG9w0BAQsFADAPMQ0wCwYDVQQDEwRDQS0w\n"
"MCIYDzIwMTQwNDA0MTk1OTA1WhgPOTk5OTEyMzEyMzU5NTlaMA8xDTALBgNVBAMT\n"
"BENBLTEwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDvhyQfsUm3T0xK\n"
"jiBXO3H6Y27b7lmCRYZQCmXCl2sUsGDL7V9biavTt3+sorWtH542/cTGDh5n8591\n"
"7rVxAB/VASmN55O3fjZyFGrjusjhXBla0Yxe5rZ/7/Pjrq84T7gc/IXiX9Sums/c\n"
"o9AeoykfhsjV2ubhh4h+8uPsHDTcAFTxq3mQaoldwnW2nmjDFzaKLtQdnyFf41o6\n"
"nsJCK/J9PtpdCID5Zb+eQfu5Yhk1iUHe8a9TOstCHtgBq61YzufDHUQk3zsT+VZM\n"
"20lDvSBnHdWLjxoea587JbkvtH8xRR8ThwABSb98qPnhJ8+A7mpO89QO1wxZM85A\n"
"xEweQlMHAgMBAAGjZDBiMA8GA1UdEwEB/wQFMAMBAf8wDwYDVR0PAQH/BAUDAwcE\n"
"ADAdBgNVHQ4EFgQU/pLsDIxE7ZF+QA2CjxiwKXwl8mQwHwYDVR0jBBgwFoAUGD0R\n"
"Yr2H7kfjQUcBMxSTCDQnhu0wDQYJKoZIhvcNAQELBQADggEBANEXLUV+Z1PGTn7M\n"
"3rPT/m/EamcrZJ3vFWrnfN91ws5llyRUKNhx6222HECh3xRSxH9YJONsbv2zY6sd\n"
"ztY7lvckL4xOgWAjoCVTx3hqbZjDxpLRsvraw1PlqBHlRQVWLKlEQ55+tId2zgMX\n"
"Z+wxM7FlU/6yWVPODIxrqYQd2KqaEp4aLIklw6Hi4HD6DnQJikjsJ6Noe0qyX1Tx\n"
"uZ8mgP/G47Fe2d2H29kJ1iJ6hp1XOqyWrVIh/jONcnTvWS8aMqS3MU0EJH2Pb1Qa\n"
"KGIvbd/3H9LykFTP/b7Imdv2fZxXIK8jC+jbF1w6rdBCVNA0p30X/jonoC3vynEK\n"
"5cK0cgs=\n"
"-----END CERTIFICATE-----\n";

static unsigned char server_key_pem[] =
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIEpQIBAAKCAQEA2d3Qs4fYDkzojm9nbaz7vdX2GIAv3stAAmI0MWq7uxWcJ7lz\n"
"4UCvSh7CFWESMqOPcIymXXZB3rdrG6RQAHhE2YfFFFi0W/5rqcmkiTMnGmAjYy2Q\n"
"lCU87b18SQgjX27YLygvlbtZrveCgT2rbJYL57Gph+wIi+cJsEUanR/2fd12hL7k\n"
"RumZNEofamTHQm6Bn47ZhhCdVfs+QkfiMh+LHJM3fFiVusAUhMIJYhml53O+cSrR\n"
"VgWVdp4za/FdUzmDnSxG4heMHblRMS+UyqtssDZEaVHytiNQXFkuQ/U3v0E+2knR\n"
"NPKy+s7ZmSISC0qzDn6C5yeJc17Kc91y5RLoZwIDAQABAoIBAQCRXAu5HPOsZufq\n"
"0K2DYZz9BdqSckR+M8HbVUZZiksDAeIUJwoHyi6qF2eK+B86JiK4Bz+gsBw2ys3t\n"
"vW2bQqM9N/boIl8D2fZfbCgZWkXGtUonC+mgzk+el4Rq/cEMFVqr6/YDwuKNeJpc\n"
"PJc5dcsvpTvlcjgpj9bJAvJEz2SYiIUpvtG4WNMGGapVZZPDvWn4/isY+75T5oDf\n"
"1X5jG0lN9uoUjcuGuThN7gxjwlRkcvEOPHjXc6rxfrWIDdiz/91V46PwpqVDpRrg\n"
"ig6U7+ckS0Oy2v32x0DaDhwAfDJ2RNc9az6Z+11lmY3LPkjG/p8Klcmgvt4/lwkD\n"
"OYRC5QGRAoGBAPFdud6nmVt9h1DL0o4R6snm6P3K81Ds765VWVmpzJkK3+bwe4PQ\n"
"GQQ0I0zN4hXkDMwHETS+EVWllqkK/d4dsE3volYtyTti8zthIATlgSEJ81x/ChAQ\n"
"vvXxgx+zPUnb1mUwy+X+6urTHe4bxN2ypg6ROIUmT+Hx1ITG40LRRiPTAoGBAOcT\n"
"WR8DTrj42xbxAUpz9vxJ15ZMwuIpk3ShE6+CWqvaXHF22Ju4WFwRNlW2zVLH6UMt\n"
"nNfOzyDoryoiu0+0mg0wSmgdJbtCSHoI2GeiAnjGn5i8flQlPQ8bdwwmU6g6I/EU\n"
"QRbGK/2XLmlrGN52gVy9UX0NsAA5fEOsAJiFj1CdAoGBAN9i3nbq6O2bNVSa/8mL\n"
"XaD1vGe/oQgh8gaIaYSpuXlfbjCAG+C4BZ81XgJkfj3CbfGbDNqimsqI0fKsAJ/F\n"
"HHpVMgrOn3L+Np2bW5YMj0Fzwy+1SCvsQ8C+gJwjOLMV6syGp/+6udMSB55rRv3k\n"
"rPnIf+YDumUke4tTw9wAcgkPAoGASHMkiji7QfuklbjSsslRMyDj21gN8mMevH6U\n"
"cX7pduBsA5dDqu9NpPAwnQdHsSDE3i868d8BykuqQAfLut3hPylY6vPYlLHfj4Oe\n"
"dj+xjrSX7YeMBE34qvfth32s1R4FjtzO25keyc/Q2XSew4FcZftlxVO5Txi3AXC4\n"
"bxnRKXECgYEAva+og7/rK+ZjboJVNxhFrwHp9bXhz4tzrUaWNvJD2vKJ5ZcThHcX\n"
"zCig8W7eXHLPLDhi9aWZ3kUZ1RLhrFc/6dujtVtU9z2w1tmn1I+4Zi6D6L4DzKdg\n"
"nMRLFoXufs/qoaJTqa8sQvKa+ceJAF04+gGtw617cuaZdZ3SYRLR2dk=\n"
"-----END RSA PRIVATE KEY-----\n";

static char ecc_key[] =
	"-----BEGIN EC PRIVATE KEY-----\n"
	"MHgCAQEEIQD9KwCA8zZfETJl440wMztH9c74E+VMws/96AVqyslBsaAKBggqhkjO\n"
	"PQMBB6FEA0IABDwVbx1IPmRZEyxtBBo4DTBc5D9Vy9kXFUZycZLB+MYzPQQuyMEP\n"
	"wFAEe5/JSLVA+m+TgllhXnJXy4MGvcyClME=\n"
	"-----END EC PRIVATE KEY-----\n";

static char ecc_cert[] =
	"-----BEGIN CERTIFICATE-----\n"
	"MIIC4DCCAoagAwIBAgIBBzAKBggqhkjOPQQDAjB9MQswCQYDVQQGEwJCRTEPMA0G\n"
	"A1UEChMGR251VExTMSUwIwYDVQQLExxHbnVUTFMgY2VydGlmaWNhdGUgYXV0aG9y\n"
	"aXR5MQ8wDQYDVQQIEwZMZXV2ZW4xJTAjBgNVBAMTHEdudVRMUyBjZXJ0aWZpY2F0\n"
	"ZSBhdXRob3JpdHkwIhgPMjAxMjA5MDEwOTIyMzZaGA8yMDE5MTAwNTA5MjIzNlow\n"
	"gbgxCzAJBgNVBAYTAkdSMRIwEAYDVQQKEwlLb2tvIGluYy4xFzAVBgNVBAsTDnNs\n"
	"ZWVwaW5nIGRlcHQuMQ8wDQYDVQQIEwZBdHRpa2kxFTATBgNVBAMTDENpbmR5IExh\n"
	"dXBlcjEXMBUGCgmSJomT8ixkAQETB2NsYXVwZXIxDDAKBgNVBAwTA0RyLjEPMA0G\n"
	"A1UEQRMGamFja2FsMRwwGgYJKoZIhvcNAQkBFg1ub25lQG5vbmUub3JnMFkwEwYH\n"
	"KoZIzj0CAQYIKoZIzj0DAQcDQgAEPBVvHUg+ZFkTLG0EGjgNMFzkP1XL2RcVRnJx\n"
	"ksH4xjM9BC7IwQ/AUAR7n8lItUD6b5OCWWFeclfLgwa9zIKUwaOBtjCBszAMBgNV\n"
	"HRMBAf8EAjAAMD0GA1UdEQQ2MDSCDHd3dy5ub25lLm9yZ4ITd3d3Lm1vcmV0aGFu\n"
	"b25lLm9yZ4IJbG9jYWxob3N0hwTAqAEBMBMGA1UdJQQMMAoGCCsGAQUFBwMBMA8G\n"
	"A1UdDwEB/wQFAwMHgAAwHQYDVR0OBBYEFKz6R2fGG0F5Elf3rAXBUOKO0A5bMB8G\n"
	"A1UdIwQYMBaAFPC0gf6YEr+1KLlkQAPLzB9mTigDMAoGCCqGSM49BAMCA0gAMEUC\n"
	"ICgq4CTInkRQ1DaFoI8wmu2KP8445NWRXKouag2WJSFzAiEAx4KxaoZJNVfBBSc4\n"
	"bA9XTz/2OnpgAZutUohNNb/tmRE=\n"
	"-----END CERTIFICATE-----\n";

static unsigned char ca_cert_pem[] =
"-----BEGIN CERTIFICATE-----\n"
"MIIC4DCCAcigAwIBAgIBADANBgkqhkiG9w0BAQsFADAPMQ0wCwYDVQQDEwRDQS0w\n"
"MCIYDzIwMTQwNDA0MTk1OTA1WhgPOTk5OTEyMzEyMzU5NTlaMA8xDTALBgNVBAMT\n"
"BENBLTAwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQD46JAPKrTsNTHl\n"
"zD06eIYBF/8Z+TR0wukp9Cdh8Sw77dODLjy/QrVKiDgDZZdyUc8Agsdr86i95O0p\n"
"w19Np3a0wja0VC9uwppZrpuHsrWukwxIBXoViyBc20Y6Ce8j0scCbR10SP565qXC\n"
"i8vr86S4xmQMRZMtwohP/GWQzt45jqkHPYHjdKzwo2b2XI7joDq0dvbr3MSONkGs\n"
"z7A/1Bl3iH5keDTWjqpJRWqXE79IhGOhELy+gG4VLJDGHWCr2mq24b9Kirp+TTxl\n"
"lUwJRbchqUqerlFdt1NgDoGaJyd73Sh0qcZzmEiOI2hGvBtG86tdQ6veC9dl05et\n"
"pM+6RMABAgMBAAGjQzBBMA8GA1UdEwEB/wQFMAMBAf8wDwYDVR0PAQH/BAUDAwcE\n"
"ADAdBgNVHQ4EFgQUGD0RYr2H7kfjQUcBMxSTCDQnhu0wDQYJKoZIhvcNAQELBQAD\n"
"ggEBALnHMubZ6WJ/XOFyDuo0imwg2onrPas3MuKT4+y0aHY943BgAOEc3jKitRjc\n"
"qhb0IUD+NS7itRwNtCgI3v5Ym5nnQoVk+aOD/D724TjJ9XaPQJzOnuGaZX99VN2F\n"
"sgwAtDXedlDQ+I6KLzLd6VW+UyWTG4qiRjOGDnG2kM1wAEOM27TzHV/YWleGjhtA\n"
"bRHxkioOni5goNlTzazxF4v9VD2uinWrIFyZmF6vQuMm6rKFgq6higAU8uesFo7+\n"
"3qpeRjNrPC4fNJUBvv+PC0WnP0PLnD/rY/ZcTYjLb/vJp1fiMJ5fU7jJklBhX2TE\n"
"tstcP7FUV5HA/s9BxgAh0Z2wyyY=\n"
"-----END CERTIFICATE-----\n";

static int read_and_send(gnutls_session_t session, struct config *cfg, FILE *fp,
		const char *peer, const char *other_peer)
{
	int retval;
	char *line = NULL;
	size_t line_len = 0, line_len_0;

	if (getline(&line, &line_len, stdin) <= 0) {
		log_e(&cfg->logger, "[%s] ERROR: Could not read line from console\n", peer);
		return -1;
	}

	if (line_len == 0)
		goto end;

	/* Make line zero-terminated, and strip the newline */
	line_len_0 = strlen(line);
	line[line_len_0 - 1] = '\0';

	do {
		/*
		 * Loop until we send all the data
		 * (I know there are better ways to do this, but...)
		 */
		retval = gnutls_record_send(session, line, line_len_0);
		if (retval < 0 && retval != GNUTLS_E_AGAIN) {
			log_e(&cfg->logger, "[%s] ERROR: Could not send data to %s", peer, other_peer);
			if (retval == GNUTLS_E_INTERRUPTED)
				log_e(&cfg->logger, " (interrupted)");
			log_e(&cfg->logger, "\n");

			goto end;
		} else {
			log_i(&cfg->logger, "[%s] Sent %d bytes to %s\n", peer, retval, other_peer);
		}
	} while (retval == GNUTLS_E_AGAIN || retval < line_len_0);

	retval = GNUTLS_E_SUCCESS;

end:
	if (line)
		free(line);
	return retval;
}

static int recv_and_print(gnutls_session_t session, struct config *cfg,
		const char *peer, const char *other_peer)
{
	char recv_buf[50];
	ssize_t recvd_bytes = 0;

	for (;;) {
		recvd_bytes = gnutls_record_recv(session, recv_buf, sizeof(recv_buf));

		if (recvd_bytes == GNUTLS_E_AGAIN) {
			break;
		} else if (recvd_bytes < 0) {
			log_i(&cfg->logger, "[%s] ** Could not read data from %s", peer, other_peer);

			if (recvd_bytes == GNUTLS_E_INTERRUPTED)
				log_i(&cfg->logger, " (interrupted)");
			else
				log_i(&cfg->logger, "\n");

			return -1;
		} else {
			log_i(&cfg->logger, "[%s] Got %ld bytes from %s: '%s'\n",
					peer, recvd_bytes, other_peer, recv_buf);
			break;
		}
	}

	return 0;
}

static void loop(gnutls_session_t session, struct config *cfg, const char *peer, int fd_socket)
{
	int fd_keyboard = fileno(stdin);
	int retval, nfds = fd_socket + 1;
	const char *other_peer = "";
	fd_set read_fds;
	struct timeval tv;

	if (!peer)
		peer = "";

	if (strcasecmp(peer, "client") == 0)
		other_peer = "server";
	else if (strcasecmp(peer, "server") == 0)
		other_peer = "client";

	do {
		/*
		 * Monitor both the keyboard and the TCP/IP socket
		 * for incoming data.
		 */
		tv.tv_sec = 0;
		tv.tv_usec = 3000;
		FD_ZERO(&read_fds);
		FD_SET(fd_socket, &read_fds);
		FD_SET(fd_keyboard, &read_fds);

		retval = select(nfds, &read_fds, NULL, NULL, &tv);

		if (retval < 0) {
			log_e(&cfg->logger, "[%s] ERROR: select(2) failed: %d\n", peer, errno);
			return;
		}

		if (retval > 0) {
			if (FD_ISSET(fd_keyboard, &read_fds) &&
					read_and_send(session, cfg, stdin, peer, other_peer) < 0)
				return;

			if (FD_ISSET(fd_socket, &read_fds) &&
					recv_and_print(session, cfg, peer, other_peer) < 0)
				return;
		}
	} while (retval >= 0);
}

static char *make_prio_string(struct config *cfg)
{
	size_t len = 150;
	char *prio = malloc(len);

	if (!prio)
		return NULL;

	strcpy(prio, "NORMAL:-VERS-ALL");
	len -= 16;
	len--;

	if (cfg->tls_13) {
		strncat(prio, ":+VERS-TLS1.3", len);
		len -= 13;
	} else {
		strncat(prio, ":+VERS-TLS1.2", len);
		len -= 13;
	}

	if (cfg->tls_false_start) {
		/*
		 * For TLS False Start we force the client into some of the below forms of Diffie-Hellman
		 * as it won't work otherwise.
		 */
		strncat(prio,
			":-KX-ALL:+DHE-RSA:+ECDHE-ECDSA:+ECDHE-RSA:-CURVE-ALL:+CURVE-X25519",
			len);
	} else if (!cfg->tls_13) {
		/* We enable SRP as an alternative form of authentication */
		strncat(prio, ":+SRP", len);
	}

	return prio;
}

static int run_gnutls_client(struct config *cfg, int argc, char **argv)
{
	int opt, retval = -1, port = 0;
	const char *false_start_data = NULL;
	unsigned int flags;
	char *prio = NULL;
	long timeout = 0;
	char *ip_addr_str = NULL, *port_str = NULL;
	struct socket s;
	gnutls_session_t session;
	gnutls_certificate_credentials_t x509_cred = NULL;
	gnutls_anon_client_credentials_t anon_cred = NULL;
	const gnutls_datum_t ca_cert = { ca_cert_pem,
		sizeof(ca_cert_pem)-1
	};
	struct option options[] = {
		{
			.name = "help",
			.has_arg = no_argument,
			.flag = NULL,
			.val = 'h'
		},
		{
			.name = "timeout",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'T'
		},
		{
			.name = "use-tls-false-start",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 0
		},
		{
			.name = "tls13",
			.has_arg = no_argument,
			.flag = NULL,
			.val = '3'
		},
		{
			.name = "ca-cert-file",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'C'
		},
		{ 0, 0, 0, 0 }
	};

	if (argc <= 2)
		goto usage;

	/* Parse command line options */
	argv++;
	argc--;
	for (;;) {
		opt = getopt_long(argc, argv,
				"3hT:C:",
				options,
				NULL);

		if (opt == -1)
			break;

		switch (opt) {
		case 0:
			cfg->tls_false_start = true;
			false_start_data = optarg;
			break;
		case '3':
			cfg->tls_13 = true;
			break;
		case 'C':
			cfg->ca_file = optarg;
			break;
		case 'T':
			timeout = strtol(optarg, NULL, 10);
			break;
		case 'h':
		default:
			goto usage;
		}
	}

	if (argc < optind) {
		log_e(&cfg->logger, "Missing IP address\n");
		goto usage;
	}

	ip_addr_str = argv[optind++];

	if (argc < optind) {
		log_e(&cfg->logger, "Missing port\n");
		goto usage;
	}

	port_str = argv[optind];
	port = strtol(port_str, NULL, 10);
	if (port <= 0) {
		log_e(&cfg->logger, "Invalid port\n");
		goto end;
	}

	memset(&s, 0, sizeof(struct socket));
	s.dst_ipv4 = inet_addr(ip_addr_str);
	s.dst_port = htons(port);

	if (client_connect(cfg, &s) != 0) {
		log_e(&cfg->logger, "Could not connect to %s:%d\n",
				ip_addr_str, port);
		goto end;
	}

	log_i(&cfg->logger, "Connected to %s:%d\n",
			ip_addr_str, port);

	/* Initialize TLS layer */
	gnutls_global_init();

	flags = GNUTLS_CLIENT;
	if (cfg->tls_false_start)
		flags |= GNUTLS_ENABLE_FALSE_START;

	if (gnutls_init(&session, flags) != GNUTLS_E_SUCCESS) {
		log_e(&cfg->logger, "Could not initialize GnuTLS\n");
		goto end;
	}
	gnutls_session_set_ptr(session, cfg);

	gnutls_handshake_set_timeout(session,
			(timeout == 0 ? GNUTLS_INDEFINITE_TIMEOUT :
					(unsigned int) timeout));

	/* Load CA certificate */
	if (cfg->ca_file) {
		log_i(&cfg->logger, "Loading CA certificate: %s\n", cfg->ca_file);
		gnutls_certificate_allocate_credentials(&x509_cred);
		gnutls_certificate_set_x509_trust_file(x509_cred,
				cfg->ca_file,
				GNUTLS_X509_FMT_PEM);
		if ((retval = gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, x509_cred))
				< 0) {
			log_e(&cfg->logger, "GnuTLS ERROR: Cannot load CA credentials (%d)\n", retval);
			goto end;
		}
	}

	/* Use anonymous credentials */
	log_i(&cfg->logger, "Using anonymous credentials\n");
	gnutls_anon_allocate_client_credentials(&anon_cred);
	if (gnutls_credentials_set(session, GNUTLS_CRD_ANON, anon_cred)
			!= GNUTLS_E_SUCCESS) {
		log_e(&cfg->logger, "GnuTLS ERROR: Cannot set client anon credentials\n");
		goto end;
	}

	/* Set priorities */
	retval = gnutls_set_default_priority(session);
	if (retval < 0) {
		log_e(&cfg->logger, "ERROR: Could not set default priorities\n");
		goto end;
	}

	prio = make_prio_string(cfg);
	if (!prio) {
		log_e(&cfg->logger, "ERROR: Could not allocate priority string\n");
		goto end;
	}

	retval = gnutls_priority_set_direct(session, prio, NULL);
	if (retval < 0) {
		log_e(&cfg->logger, "GnuTLS ERROR: Cannot set TLS 1.3 priorities (%d)\n",
				retval);
		goto end;
	}

	gnutls_transport_set_int(session, s.sock);

	/* Run TLS handshake */
	do {
		retval = gnutls_handshake(session);
	} while (retval < 0 && !gnutls_error_is_fatal(retval));

	if (gnutls_error_is_fatal(retval)) {
		log_e(&cfg->logger, "GnuTLS fatal error: %d. Aborting.\n", retval);
		goto end;
	}

	/* Check whether TLS False Start was enabled on this session */
	flags = gnutls_session_get_flags(session);
	if (flags & GNUTLS_SFLAGS_FALSE_START) {
		log_i(&cfg->logger, "Using TLS False start\n");

		/* Send false start data */
		retval = gnutls_record_send(session, false_start_data, strlen(false_start_data) + 1);
		if (retval > 0)
			log_i(&cfg->logger, "[client] Sent %d bytes via TLS False Start\n", retval);
		else
			log_e(&cfg->logger, "[client] ERROR: Could not send data via TLS False Start\n");
	} else {
		log_i(&cfg->logger, "TLS False start disabled\n");
	}

	char *desc = gnutls_session_get_desc(session);
	log_i(&cfg->logger, "[client] Handshake established successfully\n");
	log_i(&cfg->logger, "[client] Session info: %s\n", desc);
	gnutls_free(desc);

	/* Read/Write some data */
	loop(session, cfg, "client", s.sock);

	retval = 0;

end:
	close(s.sock);
	gnutls_deinit(session);
	if (x509_cred)
		gnutls_certificate_free_credentials(x509_cred);
	if (anon_cred)
		gnutls_anon_free_client_credentials(anon_cred);
	gnutls_global_deinit();
	if (prio)
		free(prio);
	return retval;

usage:
	fprintf(stderr, "Usage: %s client [args] <IP address> <port>\n", argv[0]);
	fprintf(stderr, "Arguments for 'client':\n\n"
			"    -h, --help                   This help text\n"
			"    -3, --tls13                  Accept TLS 1.3 or below\n"
			"    -f, --filename               SQLite file\n"
			"    -T, --timeout                Max. time to wait for peer response (0: wait forever)\n"
			"    -C, --ca-cert-file           CA's X.509 (root) certificate file\n"
			"        --use-tls-false-start <data>   Attempt TLS False Start with the supplied data\n");
	return 0;
}

static int run_gnutls_server(struct config *cfg, int argc, char **argv)
{
	int opt, retval = -1, port = 0;
	long timeout = 0;
	char *ip_addr = NULL;
	char *prio = NULL;
	unsigned int flags;
	gnutls_session_t session;
	gnutls_dh_params_t dh_params = NULL;
	gnutls_certificate_credentials_t x509_cred = NULL;
	const gnutls_datum_t server_cert = { server_cert_pem,
		sizeof(server_cert_pem)-1
	};
	const gnutls_datum_t server_key = { server_key_pem,
		sizeof(server_key_pem)-1
	};
	const gnutls_datum_t server_ecc_cert =
		{(unsigned char*) ecc_cert, sizeof(ecc_cert)-1};
	const gnutls_datum_t server_ecc_key =
		{(unsigned char*) ecc_key, sizeof(ecc_key)-1};
	struct socket s;
	struct in_addr dst_ipv4;
	cred_t crd = NULL;
	enum {
		AUTH_NOAUTH,
		AUTH_PSK,
		AUTH_SRP
	} auth_scheme = AUTH_NOAUTH;
	struct option options[] = {
		{
			.name = "help",
			.has_arg = no_argument,
			.flag = NULL,
			.val = 'h'
		},
		{
			.name = "filename",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'f'
		},
		{
			.name = "timeout",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'T'
		},
		{
			.name = "port",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'p'
		},
		{
			.name = "auth",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'A'
		},
		{
			.name = "ca-cert-file",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'C'
		},
		{
			.name = "cert-file",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'c'
		},
		{
			.name = "priv-key",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'k'
		},
		{
			.name = "use-tls-false-start",
			.has_arg = no_argument,
			.flag = NULL,
			.val = 0
		},
		{
			.name = "tls13",
			.has_arg = no_argument,
			.flag = NULL,
			.val = '3'
		},
		{ 0, 0, 0, 0 }
	};

	if (argc <= 2)
		goto usage;

	/* Parse command line options. */
	for (;;) {
		opt = getopt_long(argc, argv,
				"3hf:p:T:A:C:c:k:",
				options,
				NULL);

		if (opt == -1)
			break;

		switch (opt) {
		case 0:
			cfg->tls_false_start = true;
			break;
		case '3':
			cfg->tls_13 = true;
			break;
		case 'A':
			if (!strcasecmp(optarg, "srp"))
				auth_scheme = AUTH_SRP;
			else if (!strcasecmp(optarg, "psk"))
				auth_scheme = AUTH_PSK;
			else {
				log_e(&cfg->logger, "Unknown auth scheme: '%s'\n", argv[1]);
				goto usage;
			}
			break;
		case 'C':
			cfg->ca_file = optarg;
			break;
		case 'c':
			cfg->cert_file = optarg;
			break;
		case 'k':
			cfg->key_file = optarg;
			break;
		case 'f':
			cfg->sqlite_filename = optarg;
			break;
		case 'p':
			port = strtol(optarg, NULL, 10);
			break;
		case 'T':
			timeout = strtol(optarg, NULL, 10);
			break;
		case 'h':
		default:
			goto usage;
		}
	}

	/* Set up the default parameters */
	if (ip_addr == NULL)
		ip_addr = DEFAULT_IP_ADDR;
	if (port == 0)
		port = DEFAULT_PORT;

	if (port < 0) {
		log_e(&cfg->logger, "Invalid port\n");
		goto end;
	}

	if (timeout < 0 || timeout > INT_MAX) {
		log_e(&cfg->logger, "Invalid timeout. No timeout will be used.\n");
		timeout = 0;
	}

	memset(&s, 0, sizeof(struct socket));
	s.src_ipv4 = inet_addr(ip_addr);
	s.src_port = htons(port);

	/* Wait for a connection */
	if (server_accept(cfg, &s) != 0)
		goto end;

	dst_ipv4.s_addr = s.dst_ipv4;
	log_i(&cfg->logger, "Got client connection from %s:%d\n",
		inet_ntoa(dst_ipv4), s.dst_port);

	/* Initialize TLS layer */
	gnutls_global_init();

	flags = GNUTLS_SERVER;
	if (cfg->tls_false_start)
		flags |= GNUTLS_ENABLE_FALSE_START;

	if (gnutls_init(&session, flags) != GNUTLS_E_SUCCESS) {
		log_e(&cfg->logger, "Could not initialize GnuTLS\n");
		goto end;
	}
	gnutls_session_set_ptr(session, cfg);

	gnutls_handshake_set_timeout(session,
			(timeout == 0 ? GNUTLS_INDEFINITE_TIMEOUT :
					(unsigned int) timeout));

	retval = gnutls_set_default_priority(session);
	if (retval < 0) {
		log_e(&cfg->logger, "ERROR: Could not set default priorities\n");
		goto end;
	}

	prio = make_prio_string(cfg);
	if (!prio) {
		log_e(&cfg->logger, "ERROR: Could not allocate priority string.\n");
		goto end;
	}

	retval = gnutls_priority_set_direct(session, prio, NULL);
	if (retval < 0) {
		log_e(&cfg->logger, "GnuTLS ERROR: Cannot set TLS 1.3 priorities (%d)\n",
				retval);
		goto end;
	}

	switch (auth_scheme) {
	case AUTH_PSK:
		log_i(&cfg->logger, "Using PSK authentication\n");
		cred_gnutls_init(&crd, session, GNUTLS_CRD_PSK);
		break;
	case AUTH_SRP:
		log_i(&cfg->logger, "Using SRP authentication\n");
		cred_gnutls_init(&crd, session, GNUTLS_CRD_SRP);
		break;
	default:
		log_i(&cfg->logger, "No authentication scheme specified\n");

		if (cfg->ca_file && cfg->cert_file && cfg->key_file) {
			log_i(&cfg->logger, "Using X.509 authentication\n"
					"CA: %s\n"
					"Cert: %s\n"
					"Priv. key: %s\n",
					cfg->ca_file, cfg->cert_file, cfg->key_file);
			gnutls_certificate_allocate_credentials(&x509_cred);
			gnutls_certificate_set_x509_trust_file(x509_cred,
					cfg->ca_file,
					GNUTLS_X509_FMT_PEM);
			gnutls_certificate_set_x509_key_file(x509_cred,
					cfg->cert_file, cfg->key_file,
					GNUTLS_X509_FMT_PEM);
			gnutls_credentials_set(session,
					GNUTLS_CRD_CERTIFICATE,
					x509_cred);
		} else {
			log_i(&cfg->logger, "Using X.509 authentication from memory\n");
			gnutls_certificate_allocate_credentials(&x509_cred);
			gnutls_certificate_set_x509_key_mem(x509_cred,
					&server_cert, &server_key,
					GNUTLS_X509_FMT_PEM);
			gnutls_certificate_set_x509_key_mem(x509_cred,
					&server_ecc_cert, &server_ecc_key,
					GNUTLS_X509_FMT_PEM);
			gnutls_credentials_set(session,
					GNUTLS_CRD_CERTIFICATE,
					x509_cred);
		}

		break;
	}

	gnutls_transport_set_int(session, s.sock_child);

	do {
		retval = gnutls_handshake(session);
	} while (retval < 0 && !gnutls_error_is_fatal(retval));

	if (gnutls_error_is_fatal(retval)) {
		log_e(&cfg->logger, "GnuTLS fatal error: %d. Aborting.\n", retval);
		goto end;
	}

	char *desc = gnutls_session_get_desc(session);
	log_i(&cfg->logger, "[server] Handshake established successfully\n");
	log_i(&cfg->logger, "[server] Session info: %s\n", desc);
	gnutls_free(desc);

	/* Check whether TLS False Start was enabled on this session */
	flags = gnutls_session_get_flags(session);
	if (flags & GNUTLS_SFLAGS_FALSE_START)
		log_i(&cfg->logger, "Using TLS False start\n");
	else
		log_i(&cfg->logger, "TLS False start disabled\n");

	loop(session, cfg, "server", s.sock_child);

	gnutls_bye(session, GNUTLS_SHUT_RDWR);

end:
	if (s.sock_child)
		close(s.sock_child);
	if (s.sock)
		close(s.sock);

	if (x509_cred)
		gnutls_certificate_free_credentials(x509_cred);
	if (dh_params)
		gnutls_dh_params_deinit(dh_params);

	if (session)
		gnutls_deinit(session);

	cred_gnutls_deinit(&crd);

	gnutls_global_deinit();

	if (prio)
		free(prio);

	return retval;

usage:
	fprintf(stderr, "Usage: %s server [args]\n", argv[0]);
	fprintf(stderr, "Arguments for 'server':\n\n"
			"    -h, --help                   This help text\n"
			"    -3, --tls13                  Accept TLS 1.3 or below\n"
			"    -f, --filename               SQLite file\n"
			"    -T, --timeout                Max. time to wait for peer response (0: wait forever)\n"
			"    -p, --port                   Port to listen on\n"
			"    -A, --auth                   Auth method. Supported values: 'srp', 'psk'\n"
			"    -C, --ca-cert-file           CA's X.509 (root) certificate file\n"
			"    -c, --cert-file              This server's X.509 certificate file\n"
			"    -k, --priv-key               This server's private key\n"
			"        --use-tls-false-start    Attempt TLS False Start\n");
	return 0;
}

static int run_sqlite_db(struct config *cfg, int argc, char **argv)
{
	int opt, retval, ret = 1;
	sqlite_t *db = NULL;
	const char *table, *username = NULL, *password = NULL;
	enum {
		SCHEME_PSK,
		SCHEME_SRP
	} scheme;
	enum {
		ACTION_CREATE_DB,
		ACTION_ADD_USER,
		ACTION_DELETE_USER
	} action;
	struct option options[] = {
		{
			.name = "help",
			.has_arg = no_argument,
			.flag = NULL,
			.val = 'h'
		},
		{
			.name = "filename",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'f'
		},
		{
			.name = "username",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'u'
		},
		{
			.name = "password",
			.has_arg = required_argument,
			.flag = NULL,
			.val = 'p'
		},
		{ 0, 0, 0, 0 }
	};

	if (argc <= 2)
		goto usage;

	if (strcmp(argv[1], "srp") == 0)
		scheme = SCHEME_SRP;
	else if (strcmp(argv[1], "psk") == 0)
		scheme = SCHEME_PSK;
	else {
		log_e(&cfg->logger, "Unknown auth scheme '%s'\n", argv[0]);
		goto end;
	}

	if (strcmp(argv[2], "create") == 0)
		action = ACTION_CREATE_DB;
	else if (strcmp(argv[2], "add") == 0)
		action = ACTION_ADD_USER;
	else if (strcmp(argv[2], "del") == 0 || strcmp(argv[1], "delete") == 0)
		action = ACTION_DELETE_USER;

	/* Parse command line options */
	for (;;) {
		opt = getopt_long(argc - 2, &argv[2],
				"hf:u:p:",
				options,
				NULL);

		if (opt == -1)
			break;

		switch (opt) {
		case 'f':
			cfg->sqlite_filename = optarg;
			break;
		case 'u':
			username = optarg;
			break;
		case 'p':
			password = optarg;
			break;
		case 'h':
		default:
			goto usage;
		}
	}

	if (!cfg->sqlite_filename) {
		log_e(&cfg->logger, "SQLite database file not provided\n");
		goto usage;
	}

	if ((retval = sqlite_open(&db, cfg->sqlite_filename)) < 0) {
		log_e(&cfg->logger,
				"Could not open or create SQLite file at '%s': %s\n",
				cfg->sqlite_filename, sqlite3_error2str(retval));
		goto end;
	}

	switch (action) {
	case ACTION_CREATE_DB:
		retval = sqlite_create_db(db);
		if (retval < 0) {
			log_e(&cfg->logger,
					"Could not create database file at '%s': %s\n",
					cfg->sqlite_filename, sqlite3_error2str(retval));
		}

		ret = 0;
		break;
	case ACTION_ADD_USER:
		table = (scheme == SCHEME_PSK ?
				SQLITE_PSK_TABLE :
				SQLITE_SRP_TABLE);

		retval = sqlite_insert_user(db, table, username, password);
		if (retval < 0) {
			log_e(&cfg->logger,
					"Could not insert user at table %s: %s\n",
					table, sqlite3_error2str(retval));
		}

		/* Success! */
		ret = 0;
		break;
	case ACTION_DELETE_USER:
		table = (scheme == SCHEME_PSK ?
				SQLITE_PSK_TABLE :
				SQLITE_SRP_TABLE);

		retval = sqlite_delete_user(db, table, username);
		if (retval < 0) {
			log_e(&cfg->logger,
					"Could not delete user from table %s: %s\n",
					table, sqlite3_error2str(retval));
		}

		/* Success! */
		ret = 0;
		break;
	}

	sqlite_close(&db);

end:
	return ret;

usage:
	/* TODO */
	return 0;
}

int main(int argc, char **argv)
{
	struct config cfg;

	memset(&cfg, 0, sizeof(struct config));

	/* Initialize the loggers */
	cfg.logger.i = stdout;
	cfg.logger.e = stderr;

	if (argc < 2)
		goto usage;

	/* Run the specified command */
	if (strcmp(argv[1], "server") == 0)
		return run_gnutls_server(&cfg, argc, argv);
	else if (strcmp(argv[1], "client") == 0)
		return run_gnutls_client(&cfg, argc, argv);
	else if (strcmp(argv[1], "db") == 0)
		return run_sqlite_db(&cfg, --argc, ++argv);
	else if (strcmp(argv[1], "-h") && strcmp(argv[1], "--help"))
		log_e(&cfg.logger, "Unknown command '%s'\n\n", argv[1]);

usage:
	fprintf(stderr, "Usage: %s server|client|db <args>\n", argv[0]);
	fprintf(stderr, "    -h or --help prints the supported arguments for each command.\n");
	return 0;
}
