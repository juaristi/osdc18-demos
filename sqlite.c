#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <gnutls/gnutls.h>
#include "credentials.h"
#include "sqlite.h"

struct sqlite_st
{
	sqlite3 *sqlite;
};

/*
 * Quick n' dirty randomness extractor using /dev/urandom.
 *
 * This is not super secure (see: http://insanecoding.blogspot.com.es/2014/05/a-good-idea-with-bad-usage-devurandom.html),
 * but serves for demo purposes.
 */
#define SALT_SIZE 16
static int gen_random_bytes(gnutls_datum_t *out)
{
	FILE *fp = fopen("/dev/urandom", "r");
	if (!fp)
		return -1;

	out->data = gnutls_malloc(SALT_SIZE);
	if (!out->data)
		return -1;
	out->size = SALT_SIZE;

	if (fread(out->data, 1, out->size, fp) < out->size)
		return -1;

	fclose(fp);
	return 0;
}

int sqlite_open(sqlite_t **db, const char *filename)
{
	int retval;

	*db = malloc(sizeof(struct sqlite_st));
	if (!*db)
		return SQLITE_ERROR;

	retval = sqlite3_open(filename, &(*db)->sqlite);
	if (retval != SQLITE_OK) {
		free(*db);
		*db = NULL;
		return retval;
	}

	return SQLITE_OK;
}

void sqlite_close(sqlite_t **db)
{
	sqlite3_close((*db)->sqlite);
	free(*db);
	*db = NULL;
}

int sqlite_create_db(sqlite_t *db)
{
	int retval;
	sqlite3_stmt *stmt = NULL;
	char *sql[] = {
		"CREATE TABLE " SQLITE_PSK_TABLE " ("
		"    username  TEXT PRIMARY KEY,"
		"    psk       BLOB)",
		"CREATE TABLE " SQLITE_SRP_TABLE " ("
		"    username  TEXT PRIMARY KEY,"
		"    salt      BLOB,"
		"    v         BLOB,"
		"    generator BLOB,"
		"    prime     BLOB)"
	};

	for (unsigned i = 0; i < 2; i++) {
		retval = sqlite3_prepare_v2(
				db->sqlite,
				sql[i],
				-1,
				&stmt,
				NULL);
		if (retval != SQLITE_OK) {
			sqlite3_finalize(stmt);
			goto end;
		}
		retval = sqlite3_step(stmt);
		if (retval != SQLITE_DONE) {
			sqlite3_finalize(stmt);
			goto end;
		}

		sqlite3_finalize(stmt);
		stmt = NULL;
	}

	retval = SQLITE_OK;
end:
	return retval;
}

int sqlite_insert_user(sqlite_t *db, const char *table,
		const char *username, const char *password)
{
	int retval;
	const char *sql;
	sqlite3_stmt *stmt = NULL;
	/*
	 * 'out' will hold our PSK or SRP
	 * 'salt' will hold the random salt used to generate those
	 */
	gnutls_datum_t out = { NULL, 0 }, salt = { NULL, 0 };
	gnutls_datum_t generator = { NULL, 0 }, prime = { NULL, 0 };

	if (!strcmp(table, SQLITE_PSK_TABLE))
		sql = "INSERT INTO " SQLITE_PSK_TABLE " (username, psk) VALUES (?, ?)";
	else if (!strcmp(table, SQLITE_SRP_TABLE))
		sql = "INSERT INTO " SQLITE_SRP_TABLE " (username, salt, v, generator, prime) "
				"VALUES (?, ?, ?, ?, ?)";
	else
		return SQLITE_NOTFOUND;

	/* Generate a random salt */
	if (gen_random_bytes(&salt) < 0)
		return SQLITE_INTERNAL;

	/* Prepare and execute the SQL statement */
	retval = sqlite3_prepare_v2(
			db->sqlite,
			sql,
			-1,
			&stmt,
			NULL);

	if (retval != SQLITE_OK) {
		if (salt.data)
			gnutls_free(salt.data);
		return retval;
	}

	retval = sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC);
	if (retval != SQLITE_OK)
		goto end;

	if (!strcmp(table, SQLITE_PSK_TABLE)) {
		/* Generate a random-looking PSK from the password, and store it in the database */
		if ((retval = cred_gnutls_generate_psk_from_password(password, &salt, &out))
				!= GNUTLS_E_SUCCESS)
			goto end;
		retval = sqlite3_bind_blob(stmt, 2,
				out.data, out.size,
				SQLITE_STATIC);
	} else {
		/* Generate a SRP verifier from the password and store it in the database */
		if ((retval = cred_gnutls_generate_srp_verifier(username, password, &salt,
				&generator, &prime,
				&out))
				!= GNUTLS_E_SUCCESS)
			goto end;
		retval = sqlite3_bind_blob(stmt, 3,
				out.data, out.size,
				SQLITE_STATIC);
		if (retval != SQLITE_OK)
			goto end;

		/* Now store the rest of the parameters */
		if ((retval = sqlite3_bind_blob(stmt, 2,
				salt.data, salt.size,
				SQLITE_STATIC)) != SQLITE_OK)
			goto end;
		if ((retval = sqlite3_bind_blob(stmt, 4,
				generator.data, generator.size,
				SQLITE_STATIC)) != SQLITE_OK)
			goto end;
		if ((retval = sqlite3_bind_blob(stmt, 5,
				prime.data, prime.size,
				SQLITE_STATIC)) != SQLITE_OK)
			goto end;
	}

	if (retval != SQLITE_OK)
		goto end;

	retval = sqlite3_step(stmt);
	if (retval != SQLITE_DONE)
		goto end;

	retval = SQLITE_OK;

end:
	if (out.data)
		gnutls_free(out.data);
	if (salt.data)
		gnutls_free(salt.data);
	if (generator.data)
		gnutls_free(generator.data);
	if (prime.data)
		gnutls_free(prime.data);
	sqlite3_finalize(stmt);
	return retval;
}

int sqlite_delete_user(sqlite_t *db, const char *table,
		const char *username)
{
	int retval;
	const char *sql;
	sqlite3_stmt *stmt = NULL;

	if (!strcmp(table, SQLITE_PSK_TABLE))
		sql = "DELETE FROM " SQLITE_PSK_TABLE " WHERE username = ?";
	else if (!strcmp(table, SQLITE_SRP_TABLE))
		sql = "DELETE FROM " SQLITE_SRP_TABLE " WHERE username = ?";
	else
		return SQLITE_NOTFOUND;

	retval = sqlite3_prepare_v2(
			db->sqlite,
			sql,
			-1,
			&stmt,
			NULL);
	if (retval != SQLITE_OK)
		return retval;

	retval = sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC);
	if (retval != SQLITE_OK)
		goto end;

	retval = sqlite3_step(stmt);
	if (retval != SQLITE_DONE)
		goto end;

	retval = SQLITE_OK;

end:
	sqlite3_finalize(stmt);
	return retval;
}
