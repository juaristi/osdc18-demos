#include <stdio.h>
#include <stdarg.h>
#include "common.h"

void log_i(struct logger *l, const char *msg, ...)
{
	va_list args;

	if (l && l->i) {
		va_start(args, msg);
		vfprintf(l->i, msg, args);
		va_end(args);
	}
}

void log_e(struct logger *l, const char *msg, ...)
{
	va_list args;

	if (l && l->e) {
		va_start(args, msg);
		vfprintf(l->e, msg, args);
		va_end(args);
	}
}
