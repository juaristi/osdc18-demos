#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include "net.h"

#define BACKLOG 5

int server_accept(struct config *cfg, struct socket *s)
{
	int flags, on = 1;
	socklen_t addrlen;
	struct sockaddr_in addr;

	s->sock = socket(AF_INET, SOCK_STREAM, 0);
	if (s->sock == -1) {
		log_e(&cfg->logger, "Could not create socket\n");
		return -errno;
	}

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = s->src_port;
	addr.sin_addr.s_addr = s->src_ipv4;

	if (setsockopt(s->sock, SOL_SOCKET, SO_REUSEPORT, &on, sizeof(on))) {
		log_e(&cfg->logger, "setsockopt() failed: %s\n", strerror(errno));
		goto error;
	}

	if (bind(s->sock, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
		log_e(&cfg->logger, "bind() failed: %s\n", strerror(errno));
		goto error;
	}

	if (listen(s->sock, BACKLOG) == -1) {
		log_e(&cfg->logger, "listen() failed: %s\n", strerror(errno));
		goto error;
	}

	addrlen = sizeof(struct sockaddr_in);
	s->sock_child = accept(s->sock, (struct sockaddr *) &addr, &addrlen);
	if (s->sock_child == -1) {
		log_e(&cfg->logger, "accept() failed: %s\n", strerror(errno));
		goto error;
	}

	if ((flags = fcntl(s->sock, F_GETFL)) < 0) {
		log_e(&cfg->logger, "ERROR: Could not get socket flags\n");
		close(s->sock);
		return -errno;
	}
	if (fcntl(s->sock, F_SETFL, flags | O_NONBLOCK) < 0) {
		log_e(&cfg->logger, "ERROR: Could not set socket to non-blocking\n");
		close(s->sock);
		return -errno;
	}

	/* Success - Copy the peer's IP address and port */
	s->dst_ipv4 = addr.sin_addr.s_addr;
	s->dst_port = addr.sin_port;
	return 0;

error:
	close(s->sock);
	s->sock = 0;
	return -errno;
}

int client_connect(struct config *c, struct socket *s)
{
	int flags;
	struct sockaddr_in addr;

	s->sock = socket(AF_INET, SOCK_STREAM, 0);
	if (s->sock == -1) {
		log_e(&c->logger, "ERROR: Could not create socket\n");
		return -errno;
	}

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_port = s->dst_port;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = s->dst_ipv4;
//	if (!inet_aton(ip_addr, &addr.sin_addr)) {
//		fprintf(stderr, "ERROR: Could not set IP address: '%s'\n", ip_addr);
//		close(c->sockfd);
//		return 1;
//	}

	if (connect(s->sock, (struct sockaddr *) &addr, sizeof(struct sockaddr_in)) == -1) {
		log_e(&c->logger, "ERROR: Could not connect to peer\n");
		close(s->sock);
		return -errno;
	}

	if ((flags = fcntl(s->sock, F_GETFL)) < 0) {
		log_e(&c->logger, "ERROR: Could not get socket flags\n");
		close(s->sock);
		return -errno;
	}
	if (fcntl(s->sock, F_SETFL, flags | O_NONBLOCK) < 0) {
		log_e(&c->logger, "ERROR: Could not set socket to non-blocking\n");
		close(s->sock);
		return -errno;
	}

	/* Success - Copy peer's IP address and port */
	s->dst_ipv4 = addr.sin_addr.s_addr;
	s->dst_port = addr.sin_port;

	return 0;
}
