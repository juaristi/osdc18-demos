#include <string.h>
#include <stdint.h>
#include <sqlite3.h>
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#include "common.h"
#include "sqlite.h"
#include "errors.h"
#include "credentials.h"

struct cred_st
{
	union {
		gnutls_psk_server_credentials_t psk_cred;
		gnutls_srp_server_credentials_t srp_cred;
	} creds;

	gnutls_credentials_type_t type;
};

static int copy_data(gnutls_datum_t *dst, const char *src, unsigned int length)
{
	dst->size = length;
	dst->data = gnutls_malloc(length);

	if (!dst->data)
		return GNUTLS_E_MEMORY_ERROR;

	memcpy(dst->data, src, length);
	return GNUTLS_E_SUCCESS;
}

static int start_query(struct config *cfg,
		const char *sql,
		sqlite3 **db, sqlite3_stmt **stmt)
{
	int ret;

	ret = sqlite3_open(cfg->sqlite_filename, db);
	if (ret != SQLITE_OK) {
		log_e(&cfg->logger,
				"Could not open file '%s'. sqlite3_open() failed with %s\n",
				cfg->sqlite_filename, sqlite3_error2str(ret));
		goto error;
	}

	ret = sqlite3_prepare_v2(
			*db,
			sql,
			-1,	/* Read SQL up until the '\0' */
			stmt,
			NULL);
	if (ret != SQLITE_OK) {
		log_e(&cfg->logger,
				"Could not prepare SQL statement. sqlite3_prepare_v2() failed with %s\n",
				sqlite3_error2str(ret));
		goto error;
	}

	return GNUTLS_E_SUCCESS;

error:
	return ret;
}

static void end_query(struct config *cfg, sqlite3 *db, sqlite3_stmt *stmt)
{
	int ret;

	if (stmt && (ret = sqlite3_finalize(stmt)) != SQLITE_OK) {
		log_e(&cfg->logger, "sqlite3_finalize() failed with %s. THIS SHOULD NOT HAPPEN!\n",
				sqlite3_error2str(ret));
	}
	if (db && (ret = sqlite3_close(db)) != SQLITE_OK) {
		log_e(&cfg->logger, "sqlite3_close() failed with %s. THIS SHOULD NOT HAPPEN!\n",
				sqlite3_error2str(ret));
	}
}

/*
 * This is the callback function to retrieve the SRP credentials.
 *
 * This function is invoked by GnuTLS and it should return one of the following values:
 *
 *  -  1 : User name does not exist.
 *  - <0 : An error occurred. GnuTLS will abort the handshake.
 *  -  0 : Success. GnuTLS will use the provided credentials.
 *
 *  This function is expected to fill the 'salt', 'verifier',
 *  'generator' and 'prime' values. These should be malloc'ed with gnutls_malloc(),
 *  and GnuTLS will take care of freeing them.
 *
 *  Note that GnuTLS will never see the password. The 'verifier' parameter expects to receive
 *  the SRP verifier value, which is computed the following way:
 *
 *       verifier = SHA1(s | SHA1(I | ":" | P))
 *
 *  where P is the password, I is the client's identity (the username) and '|' denotes concatenation.
 *
 *  We don't have to compute that value however, as it's been precomputed and stored in the database.
 *  We just read it from there.
 */
static int srp_creds_cb(gnutls_session_t session,
		const char *username,
		gnutls_datum_t *salt, gnutls_datum_t *verifier,
		gnutls_datum_t *generator, gnutls_datum_t *prime)
{
	char sql[] = "SELECT salt, v, generator, prime FROM " SQLITE_SRP_TABLE " WHERE username = ?";
	int ret, retval = -1;
	int s_len, v_len, g_len, p_len;
	const char *s = NULL, *v = NULL, *g = NULL, *p = NULL;
	sqlite3 *sqlite_db = NULL;
	sqlite3_stmt *stmt = NULL;
	struct config *cfg = NULL;

	cfg = gnutls_session_get_ptr(session);
	if (!cfg)
		goto end;	/* This should not happen! */
	if (!cfg->sqlite_filename) {
		log_e(&cfg->logger,
				"SQLite database filename not set!\n");
		goto end;
	}

	if (start_query(cfg, sql, &sqlite_db, &stmt) != GNUTLS_E_SUCCESS)
		goto end;

	if ((ret = sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC)) != SQLITE_OK)
		goto end;

	if ((ret = sqlite3_step(stmt)) == SQLITE_ROW) {
		/*
		 * We've got a row. Retrieve its hashed password, which is expected to be correctly
		 * formatted so that it can be directly fed to the verifier ('v') value of SRP.
		 *
		 * Take its length, allocate memory for the 'verifier' gnutls_datum_t, and copy
		 * the whole blob there.
		 */
		s = sqlite3_column_blob(stmt, 0);
		v = sqlite3_column_blob(stmt, 1);
		g = sqlite3_column_blob(stmt, 2);
		p = sqlite3_column_blob(stmt, 3);
	} else if (ret != SQLITE_DONE) {
		log_e(&cfg->logger, "sqlite3_step() failed with %s\n",
				sqlite3_error2str(ret));
	}

	if (!s || !v || !g || !p) {
		/* Username not found! */
		log_i(&cfg->logger, "GnuTLS SRP: username '%s' not found in database\n",
				username);
		retval = 1;
	} else {
		/*
		 * The 'v' pointer is valid until we call sqlite3_finalize().
		 * SQLite will free it for us.
		 */
		s_len = sqlite3_column_bytes(stmt, 0);
		v_len = sqlite3_column_bytes(stmt, 1);
		g_len = sqlite3_column_bytes(stmt, 2);
		p_len = sqlite3_column_bytes(stmt, 3);
		if (s_len <= 0 || v_len <= 0 || g_len <= 0 || p_len <= 0) {
			log_e(&cfg->logger,
					"sqlite3_column_bytes() returned %s, but we've got a valid blob! THIS SHOULD NOT HAPPEN!\n",
					sqlite3_error2str(retval));
			goto end;
		}

		/* Return all params to caller */
		if (copy_data(salt, s, s_len) != GNUTLS_E_SUCCESS)
			goto end;
		if (copy_data(verifier, v, v_len) != GNUTLS_E_SUCCESS)
			goto end;
		if (copy_data(generator, g, g_len) != GNUTLS_E_SUCCESS)
			goto end;
		if (copy_data(prime, p, p_len) != GNUTLS_E_SUCCESS)
			goto end;

		/* Success! */
		retval = 0;
	}

end:
	if (stmt && (ret = sqlite3_finalize(stmt)) != SQLITE_OK) {
		log_e(&cfg->logger, "sqlite3_finalize() failed with %s. THIS SHOULD NOT HAPPEN!\n",
				sqlite3_error2str(ret));
	}
	if (sqlite_db && (ret = sqlite3_close(sqlite_db)) != SQLITE_OK) {
		log_e(&cfg->logger, "sqlite3_close() failed with %s. THIS SHOULD NOT HAPPEN!\n",
				sqlite3_error2str(ret));
	}

	return retval;
}

/*
 * This is the callback function to retrieve the pre-shared key (PSK)
 * associated to the given username.
 *
 * This function is invoked by GnuTLS and it should return one of the following values:
 *
 *  - -1 : Error. GnuTLS will act as if the username did not exist, and will generate a random PSK.
 *  -  0 : Success. GnuTLS will use the provided PSK.
 *
 * When GnuTLS assumes the given username does not exist, it will generate a random PSK and continue
 * the handshake as if it was successful, rather than generating an alert and aborting the handshake.
 * This will, with very high probability (read: always), cause the handshake to fail in the end since
 * the other peer will be unable to generate the same master secret.
 */
static int psk_creds_sb(gnutls_session_t session,
		const char *username, gnutls_datum_t *key)
{
	char sql[] = "SELECT psk FROM " SQLITE_PSK_TABLE " WHERE username = ?";
	int ret, retval = -1;
	const char *psk = NULL;
	sqlite3 *sqlite_db = NULL;
	sqlite3_stmt *stmt = NULL;
	struct config *cfg = NULL;

	cfg = gnutls_session_get_ptr(session);
	if (!cfg)
		goto end;
	if (!cfg->sqlite_filename) {
		log_e(&cfg->logger,
				"SQLite database filename not set!\n");
		goto end;
	}

	if (start_query(cfg, sql, &sqlite_db, &stmt) != GNUTLS_E_SUCCESS)
		goto end;

	if ((ret = sqlite3_bind_text(stmt, 1, username, -1, SQLITE_STATIC)) != SQLITE_OK)
		goto end;

	if ((ret = sqlite3_step(stmt)) == SQLITE_ROW) {
		/* We've got a row. Retrieve the PSK directly, in a blob. */
		psk = sqlite3_column_blob(stmt, 0);
	}

	if (!psk) {
		/*
		 * Return an error, but log that the username was not found.
		 * GnuTLS will generate a random PSK.
		 */
		log_i(&cfg->logger,
				"GnuTLS PSK: username '%s' not found in database\n",
				username);
		goto end;
	} else {
		/* 'psk' is valid until we call sqlite3_finalize() - SQLite will free it for us */
		ret = sqlite3_column_bytes(stmt, 0);
		if (ret <= 0) {
			log_e(&cfg->logger,
					"sqlite3_column_bytes() returned %s, but we've got a valid blob! THIS SHOULD NOT HAPPEN!\n",
					sqlite3_error2str(ret));
			goto end;
		}

		if (copy_data(key, psk, ret) != GNUTLS_E_SUCCESS)
			goto end;

		/* Success! */
		retval = 0;
	}

end:
	end_query(cfg, sqlite_db, stmt);

	return retval;
}

int cred_gnutls_init(cred_t *crdp, gnutls_session_t session, gnutls_credentials_type_t type)
{
	int retval;
	struct cred_st *crd;

	if (!crdp)
		return GNUTLS_E_INVALID_REQUEST;

	crd = gnutls_calloc(1, sizeof(struct cred_st));
	if (!crd)
		return GNUTLS_E_MEMORY_ERROR;

	crd->type = type;

	switch (type) {
	case GNUTLS_CRD_PSK:
		gnutls_psk_allocate_server_credentials(&crd->creds.psk_cred);
		gnutls_psk_set_server_credentials_function(crd->creds.psk_cred, psk_creds_sb);
		retval = gnutls_credentials_set(session, GNUTLS_CRD_PSK, crd->creds.psk_cred);
		break;
	case GNUTLS_CRD_SRP:
		gnutls_srp_allocate_server_credentials(&crd->creds.srp_cred);
		gnutls_srp_set_server_credentials_function(crd->creds.srp_cred, srp_creds_cb);
		retval = gnutls_credentials_set(session, GNUTLS_CRD_SRP, crd->creds.srp_cred);
		break;
	default:
		retval = GNUTLS_E_INVALID_REQUEST;
		break;
	}

	if (retval == GNUTLS_E_SUCCESS)
		*crdp = crd;
	return retval;
}

void cred_gnutls_deinit(cred_t *crd)
{
	if (!crd || !*crd)
		return;

	if ((*crd)->type == GNUTLS_CRD_PSK)
		gnutls_psk_free_server_credentials((*crd)->creds.psk_cred);
	else if ((*crd)->type == GNUTLS_CRD_SRP)
		gnutls_srp_free_server_credentials((*crd)->creds.srp_cred);

	gnutls_free(*crd);
	*crd = NULL;
}

#define CTX_STRING "This is a PSK"
int cred_gnutls_generate_psk_from_password(const char *password,
		const gnutls_datum_t *salt,
		gnutls_datum_t *out)
{
	int retval;
	gnutls_hash_hd_t hd;
	gnutls_digest_algorithm_t algo = GNUTLS_DIG_SHA256;

	out->size = gnutls_hash_get_len(algo);
	if (out->size == 0)
		return GNUTLS_E_INTERNAL_ERROR;
	out->data = gnutls_malloc(out->size);
	if (!out->data)
		return GNUTLS_E_MEMORY_ERROR;

	if ((retval = gnutls_hash_init(&hd, algo)) != GNUTLS_E_SUCCESS)
		return retval;

	/*
	 * Straightforward pre-shared key generation by hashing the password and salt together.
	 *
	 * I won't bother using PBKDF2 or scrypt, as it won't make a difference.
	 * Bear in mind that the resulting pseudo-random string will be used directly as the PSK,
	 * so being very hard to reverse doesn't add extra security.
	 */
	if ((retval = gnutls_hash(hd, password, strlen(password))) != GNUTLS_E_SUCCESS)
		goto end;
	if ((retval = gnutls_hash(hd, CTX_STRING, sizeof(CTX_STRING) - 1)) != GNUTLS_E_SUCCESS)
		goto end;
	if ((retval = gnutls_hash(hd, salt->data, salt->size)) != GNUTLS_E_SUCCESS)
		goto end;

	retval = GNUTLS_E_SUCCESS;
end:
	gnutls_hash_deinit(hd, out->data);
	return retval;
}

int cred_gnutls_generate_srp_verifier(const char *username, const char *password,
		const gnutls_datum_t *salt,
		gnutls_datum_t *generator, gnutls_datum_t *prime,
		gnutls_datum_t *out)
{
	/* Copy the generator and prime we'll be using */
	if (copy_data(generator,
			gnutls_srp_1024_group_generator.data,
			gnutls_srp_1024_group_generator.size) != GNUTLS_E_SUCCESS)
		return GNUTLS_E_MEMORY_ERROR;
	if (copy_data(prime,
			gnutls_srp_1024_group_prime.data,
			gnutls_srp_1024_group_prime.size) != GNUTLS_E_SUCCESS)
		return GNUTLS_E_MEMORY_ERROR;
	return gnutls_srp_verifier(username, password, salt, generator, prime, out);
}
