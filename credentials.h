#ifndef CREDENTIALS_H
#define CREDENTIALS_H
#include <gnutls/gnutls.h>

/* These functions initialize the GnuTLS credential callbacks */
struct cred_st;
typedef struct cred_st *cred_t;

int  cred_gnutls_init(cred_t *, gnutls_session_t, gnutls_credentials_type_t);
void cred_gnutls_deinit(cred_t *);

int cred_gnutls_generate_psk_from_password(const char *,		/* password */
		const gnutls_datum_t *,					/* salt */
		gnutls_datum_t *);					/* output buffer */

int cred_gnutls_generate_srp_verifier(const char *, const char *,	/* username and password */
		const gnutls_datum_t *,					/* salt */
		gnutls_datum_t *, gnutls_datum_t *,			/* generator and prime output buffers */
		gnutls_datum_t *);					/* output buffer */
#endif
