#ifndef COMMON_H
#define COMMON_H
#include <stdio.h>
#include <stdbool.h>
#include <gnutls/gnutls.h>

struct logger {
		FILE *i;
		FILE *e;
};

struct config
{
	const char *sqlite_filename;
	const char
		*ca_file,
		*cert_file,
		*key_file;
	struct logger logger;

	bool tls_13;
	bool tls_false_start;
};

void log_i(struct logger *, const char *, ...) __attribute__ ((format (printf, 2, 3)));
void log_e(struct logger *, const char *, ...) __attribute__ ((format (printf, 2, 3)));

#endif
