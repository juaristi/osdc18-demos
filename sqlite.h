#ifndef SQLITE_H_
#define SQLITE_H_

#define SQLITE_PSK_TABLE "psk_users"
#define SQLITE_SRP_TABLE "srp_users"

struct sqlite_st;
typedef struct sqlite_st sqlite_t;

int  sqlite_open(sqlite_t **, const char *);
void sqlite_close(sqlite_t **);

int sqlite_create_db(sqlite_t *);

int sqlite_insert_user(sqlite_t *, const char *,	/* table name: PSK or SRP */
		const char *, const char *);		/* username and password */
int sqlite_delete_user(sqlite_t *, const char *,	/* table name: PSK or SRP */
		const char *);				/* username */

#endif
