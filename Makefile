OUT = main
all: common.h errors.h credentials.c log.c net.c net.h sqlite.c sqlite.h main.c
ifdef GNUTLS_PATH
	gcc -g -O0 -o $(OUT) -I$(GNUTLS_PATH)/include -L$(GNUTLS_PATH)/lib -Wl,-rpath=$(GNUTLS_PATH)/lib $^ -lsqlite3 -lgnutls
else
	gcc -g -O0 -o $(OUT) $^ -lsqlite3 -lgnutls
endif

clean: $(OUT)
	@rm $(OUT)
